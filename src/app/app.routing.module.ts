
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// import { GraficosComponent } from './dashboard/components/graficos/graficos.component';
// import { CadastroComponent, ListagemComponent } from './cadastros/cursos/components';
// import { GraficosComponent } from './dashboard/components/graficos/graficos.component';

const routes: Routes = [
    {   
        path: '', 
        redirectTo: 'autenticacao/login', 
        pathMatch: 'full' 
    }
    // { 
    //     path: 'dashboard', 
    //     component: GraficosComponent,
    // }
    // { 
    //     path: 'cadastros/cursos/listagem', 
    //     component: ListagemComponent,
    // },
    // { 
    //     path: 'cadastros/cursos/cadastro', 
    //     component: CadastroComponent,
    // },
    // { 
    //     path: 'p/add', 
    //     component: PhotoFormComponent 
    // },
    // { 
    //     path: '**', 
    //     component: NotFoundComponent 
    // }  
];

@NgModule({
    imports: [ 
        RouterModule.forRoot(routes) 
    ],
    exports: [ RouterModule ]
})
export class AppRoutingModule { }