import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { CursoListagemComponent } from './components/curso-listagem/curso-listagem.component';
import { CursoCadastroComponent } from './components/curso-cadastro/curso-cadastro.component';
import { CursoComponent } from './components';
import { HttpUtilService, CursoService } from '../shared';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import {
  MatInputModule,
  MatButtonModule,
  MatListModule,
  MatTooltipModule,
  MatIconModule,
  MatSnackBarModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatCardModule,
  MatPaginatorIntl
} from '@angular/material';

import { SharedModule } from '../shared/shared.module';
import { CursoEditarComponent } from './components/curso-editar/curso-editar.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    FormsModule,
    MatInputModule,
    MatButtonModule,
    MatListModule,
    MatTooltipModule,
    MatIconModule,
    MatSnackBarModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatCardModule,
    SharedModule
  ],
  declarations: [
    CursoListagemComponent, 
    CursoCadastroComponent,
    CursoComponent,
    CursoEditarComponent
  ],
  providers: [
    HttpUtilService,
    CursoService
  ]
})
export class CursoModule { }
