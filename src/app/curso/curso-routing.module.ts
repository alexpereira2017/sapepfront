import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {
    CursoCadastroComponent,
    CursoListagemComponent,
    CursoComponent,
    CursoEditarComponent
} from './components';

export const CursoRoutes: Routes = [
    {
      path: 'curso',
      component: CursoComponent,
      children: [
        {
          path: 'listagem', 
          component: CursoListagemComponent
        },
        {
          path: 'cadastro', 
          component: CursoCadastroComponent 
        },
        {
          path: 'editar/:id',
          component: CursoEditarComponent
        },
      ]
    }
];

@NgModule({
    imports: [
      RouterModule.forChild(CursoRoutes)
    ],
    exports: [
      RouterModule
    ]
  })
  export class CursoRoutingModule {
  }
  