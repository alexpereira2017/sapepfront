import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { MatSnackBar } from '@angular/material';
import {NgForm, FormGroup, FormBuilder, Validators} from '@angular/forms';
import { Curso } from 'src/app/shared/models';
import { CursoService } from 'src/app/shared';
import { Observable } from 'rxjs';
import {map, defaultIfEmpty} from 'rxjs/operators';

@Component({
  selector: 'app-curso-editar',
  templateUrl: './curso-editar.component.html',
  styleUrls: ['./curso-editar.component.css']
})
export class CursoEditarComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private snackBar: MatSnackBar,
    private cursoService: CursoService
  ) { }
  curso: any = {};
  msgErro = '';
  msgAviso = '';
  msgSucesso = '';

  ngOnInit() {
    const id: Observable<string> = this.route.params.pipe(map(p => p.id),defaultIfEmpty(null));    
    this.route.params.subscribe(params => {
      this.cursoService.listarCurso(params.id).subscribe(res => {
        this.curso = res;
      });
    });    
  }

  raizCurso() {
    this.router.navigate(['/curso/listagem']);
  }

  salvar(FRMcadastro) {
    const curso: Curso = FRMcadastro.form.value;
    this.cursoService.editarCurso(this.curso.id, curso)
      .subscribe(
        data => {
          this.msgSucesso = "Cadastro atualizado com sucesso.";
        },
        err => {
          if (err.status == 400) {
            this.msgAviso = err.error.errors.join(' ');
          } else {
            this.msgErro =  err.message; 
          }
        }
      );
  	return false;
  }
}