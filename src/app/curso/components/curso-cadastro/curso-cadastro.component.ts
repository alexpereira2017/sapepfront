import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { 
  MatTableDataSource,
  MatSnackBar,
  MatSnackBarModule ,
  PageEvent,
  MatPaginator,
  Sort,
  MatSort
} from '@angular/material';
import {NgForm, FormGroup, FormBuilder, Validators} from '@angular/forms';
import { Curso } from 'src/app/shared/models';
import { CursoService } from 'src/app/shared';
import { Observable } from 'rxjs';
import {map, defaultIfEmpty} from 'rxjs/operators';

@Component({
  selector: 'app-curso-cadastro',
  templateUrl: './curso-cadastro.component.html',
  styleUrls: ['./curso-cadastro.component.css']
})
export class CursoCadastroComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private snackBar: MatSnackBar,
    private cursoService: CursoService
  ) { }
  nome: string;
  situacao: string;
  curso: any = {};

  ngOnInit() {}

  raizCurso() {
    this.router.navigate(['/curso/listagem']);
  }

  onSubmit(form: NgForm) {
    console.log('Your form data : ', form.value );
  }

  cadastrarCurso(FRMcadastro) {
    const curso: Curso = FRMcadastro.form.value;
    if (FRMcadastro.invalid) {
      return false;
    }
    this.cursoService.cadastrarCurso(curso)
      .subscribe(
        data => {
          const msg: string = "Cadastro realizado com sucesso.";
          this.snackBar.open(msg, "Sucesso", { duration: 5000 });
          this.router.navigate(['/curso/listagem']);
        },
        err => {
          let msg: string = "Tente novamente em instantes.";
          if (err.status == 400) {
            msg = err.error.errors.join(' ');
          }
          this.snackBar.open(msg, "Erro", { duration: 5000 });
        }
      );
  	return false;
  }




}
