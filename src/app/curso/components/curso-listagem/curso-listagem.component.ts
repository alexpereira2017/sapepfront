import { Component, OnInit, ViewChild } from '@angular/core';
import {Router} from '@angular/router';
import {
  HttpUtilService,
  CursoService
} from '../../../shared';
import { Curso } from 'src/app/shared/models';
import { 
  MatTableDataSource,
  MatSnackBar,
  MatSnackBarModule ,
  PageEvent,
  MatPaginator,
  Sort,
  MatSort
} from '@angular/material';

@Component({
  selector: 'app-curso-listagem',
  templateUrl: './curso-listagem.component.html',
  styleUrls: ['./curso-listagem.component.css']
})
export class CursoListagemComponent implements OnInit {

  dataSource: MatTableDataSource<Curso>;
  colunas: string[] = ['nome', 'data'];
  cursos: Curso[];

  @ViewChild(MatSort,{ static: true }) sort: MatSort;
  @ViewChild(MatPaginator,{ static: true }) paginator: MatPaginator;
  
  constructor(
    private router: Router,
    // private httpUtil: HttpUtilService,
    private cursoService: CursoService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.buscarTodosCursos();
  }

  novoCurso() {
    this.router.navigate(['/curso/cadastro']);
  }

  buscarTodosCursos() {
    this.cursoService.listarTodosCursos()
      .subscribe((data: Curso[]) => {
        this.cursos = data;
      });
    //   err => {
    //     const msg: string = "Erro obtendo lançamentos.";
    //     this.snackBar.open(msg, "Erro", { duration: 5000 });
    //   }
    // );
  }

  remover(curso: Curso) {
    this.cursoService.removerCurso(curso.id)
    .subscribe( data => {
      this.cursos = this.cursos.filter(u => u !== curso);
    })
  }


}
