import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {
    LancarDadosComponent,
    LancarDadosEditarComponent,
    LancarDadosListagemComponent
} from './components';

export const EixoRoutes: Routes = [
    {
      path: 'acoes/lancar-dados',
      component: LancarDadosComponent,
      children: [
        {
          path: 'listagem', 
          component: LancarDadosListagemComponent
        },
        {
          path: 'editar/:id',
          component: LancarDadosEditarComponent  
        },
      ]
    }
];

@NgModule({
    imports: [
      RouterModule.forChild(EixoRoutes)
    ],
    exports: [
      RouterModule
    ]
  })
  export class LancarDadosRoutingModule {
  }
  