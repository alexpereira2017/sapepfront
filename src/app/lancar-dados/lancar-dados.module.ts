import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LancarDadosListagemComponent } from './components/lancar-dados-listagem/lancar-dados-listagem.component';
import { LancarDadosEditarComponent } from './components/lancar-dados-editar/lancar-dados-editar.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VMessageModule } from '../shared/components/vmessage/vmessage.module';
import { SharedModule } from '../shared/shared.module';
import { LancarDadosComponent } from './components';
import { CmpModule } from '../shared/components/cmp-indicadores';
import { CmpTextareaComponent } from '../shared/components/cmp-indicadores/cmp-textarea';
import { CmpTextoComponent } from '../shared/components/cmp-indicadores/cmp-texto';
import { CmpNumeroComponent } from '../shared/components/cmp-indicadores/cmp-numero';
import { 
  CardStyle1Component, CardStyle2Component, 
  CardStyle3Component, CardStyle4Component, 
  CardStyle5Component, CardStyle6Component 
} from '../shared/components/indicadores/card-templates';
import { IndicadoresModule } from '../shared/components/indicadores';
import { Ng2SmartTableModule } from 'ng2-smart-table';


@NgModule({
  declarations: [
    LancarDadosComponent,
    LancarDadosListagemComponent, 
    LancarDadosEditarComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    VMessageModule,
    IndicadoresModule,
    SharedModule,
    Ng2SmartTableModule,
    ReactiveFormsModule
  ],  
  entryComponents: [
    CmpTextareaComponent,
    CmpTextoComponent, 
    CmpNumeroComponent,
    CardStyle1Component, 
    CardStyle2Component, 
    CardStyle3Component, 
    CardStyle4Component, 
    CardStyle5Component, 
    CardStyle6Component
  ],

})
export class LancarDadosModule { }
