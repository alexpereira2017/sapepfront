import { Component, OnInit,
         ViewChild,
         ViewContainerRef,
         ComponentFactoryResolver,
         ComponentRef,
         QueryList,
         ComponentFactory } from '@angular/core';
import { AutoavaliacaoService } from 'src/app/shared';
import { Router, ActivatedRoute } from '@angular/router';
import { Autoavaliacao, Eixo, Resposta, Indicador } from 'src/app/shared/models';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { CmpModule } from '../../../shared/components/cmp-indicadores';
import { CmpTextoComponent } from 'src/app/shared/components/cmp-indicadores/cmp-texto';
import { CmpTextareaComponent } from 'src/app/shared/components/cmp-indicadores/cmp-textarea';
import { CardStyle1Component } from 'src/app/shared/components/indicadores/card-templates/card-style-1'
import { RespostaService } from 'src/app/shared/services/resposta.service';
import { LocalDataSource } from 'ng2-smart-table';
import { MetaService } from 'src/app/shared/services/meta.service';


@Component({
  selector: 'app-lancar-dados-editar',
  templateUrl: './lancar-dados-editar.component.html',
  styleUrls: ['./lancar-dados-editar.component.css']
})
export class LancarDadosEditarComponent implements OnInit {
  respostas: Resposta[] = [];
  FRMcadastro: FormGroup;
  id?: string; 
  autoavaliacao: Autoavaliacao;
  componentRef: any;
  @ViewChild('loadComponent', { read: ViewContainerRef, static: true }) entry: ViewContainerRef;
   
  settings = {
    noDataMessage: 'Nenhuma meta cadastrada',
    pager: {
      display : false
    },
    actions: {
      position: 'right',
      columnTitle: 'Ações'
    },
    add: {
      // addButtonContent: 'Nova meta',
      // createButtonContent: 'Salvar',
      // cancelButtonContent: 'Cancelar',
      addButtonContent: '<i class="fa fa-plus" title="Nova Meta"></i>',
      createButtonContent: '<i class="fa fa-check" title="Salvar"></i>',
      cancelButtonContent: '<i class="fa fa-close" title="Cancelar"></i>',
      confirmCreate: true
    },
    edit: {
      // editButtonContent: 'Editar',
      editButtonContent: '<i class="fa fa-pencil" title="Editar"></i>',
      saveButtonContent: 'Salvar',
      cancelButtonContent: 'Cancelar',
      confirmSave: true,
    },
    delete: {
      // deleteButtonContent: 'Remover',
      deleteButtonContent: '<i class="fa fa-trash-o"></i>',
      confirmDelete: true
    },
    columns: {
      id: {
        title: 'Cód.',
        editable: false,
        width: '5%',
        
        sort: false,
      },
      meta: {
        title: 'Metas',
        editor: {type: 'textarea'},
        width: '18%',
        sort: false,
      },
      pontoFraco: {
        title: 'Pontos Fracos',
        editor: {type: 'textarea'},
        width: '18%',
        sort: false
      },
      pontoForte: {
        title: 'Pontos Fortes',
        editor: {type: 'textarea'},
        width: '18%',
        sort: false
      },
      ameacaInterna: {
        title: 'Ameaças Internas',
        editor: {type: 'textarea'},
        width: '18%',
        sort: false
      },
      ameacaExterna: {
        title: 'Ameaças Externas',
        editor: {type: 'textarea'},
        width: '18%',
        sort: false
      },
      alcancado: {
        title: 'Alcançado',
        width: '5%',
        sort: false
      }
    }
  };

  source: LocalDataSource;

  constructor(
    private route: ActivatedRoute,
    private autoavaliacaoService: AutoavaliacaoService,
    private formBuilder: FormBuilder,
    private resolver: ComponentFactoryResolver,
    private respostaService: RespostaService,
    private metaService: MetaService
  ) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.buscar();
    // this.createComponent(2);
    this.FRMcadastro = this.formBuilder.group({
      autoavaliacao: [''],
      conceito: ['']
    });
  }

  buscar() {
    this.autoavaliacaoService.listarDetalhado(this.id)
      .subscribe((data: Autoavaliacao) => {
        this.autoavaliacao = data;
        console.log(this.autoavaliacao);
        this.adicionarIndicadoresAoFormulario(this.autoavaliacao.eixos, this.autoavaliacao.anos);
      });
    }
    
  adicionarIndicadoresAoFormulario(eixos: Eixo[], anosCiclo: number[]) {
    eixos.map(
      eixo => {
        let indicadoresAgrupados = Object.values(eixo.indicadoresAgrupados);
        indicadoresAgrupados.map(
          arrIndicador => {
            arrIndicador.map(
              indicador => {
                if (indicador.temporalidade == 'RE') {
                  anosCiclo.map(ano => this.montarFormulario(indicador,ano))
                } else {
                  this.montarFormulario(indicador,'');
                }
            });
        });
    });
  }

  montarFormulario(indicador, ano?) {
    const ref = 'indicador_'+indicador.id+ano;
    let resposta;
    
    resposta = indicador.respostas.filter(r => r.ano == ano);
    
    if (resposta == '') {
      resposta = indicador.respostas;
    }

    this.FRMcadastro.addControl(ref, new FormControl(resposta[0].descritiva));
    // ou --> this.FRMcadastro.get(ref).setValue(55);

    this.respostas.push(
      new Resposta(ano,"","","",indicador.id,resposta[0].id)
    );
  }

  cadastrar() {
    
    if (this.FRMcadastro.invalid) {
      return false;
    }

    this.respostas.forEach(r => {
        let complemento = r.ano != null ? r.ano : '';
        const CMPform = this.FRMcadastro.get('indicador_'+r.cod_indicador+complemento);
        r.descritiva = CMPform.value;
    });
    
    this.respostaService.salvar(this.respostas)
      .subscribe(
        data => {
          const msg: string = "Cadastro realizado com sucesso.";
        },
        err => {
          let msg: string = "Tente novamente em instantes.";
          msg.concat(err.error.message);
        }
      );
  	return false;
  }

  criarMeta(event, eixoId: number){
    event.newData.cod_eixo = eixoId;
    this.metaService.inserir(event.newData).subscribe(
      data => {
        event.newData = data;
        // event.newData.id = 2;
        // const msg: string = "Cadastro realizado com sucesso.";
        event.confirm.resolve(event.newData);
      },
      err => {
        let msg: string = "Tente novamente em instantes.";
        msg.concat(err.error.message);
      }
    );
    
    
  }
  editarMeta(event, eixoId: number){

    console.log('editar');
    console.log(event);
    console.log(event.target);
    event.newData.cod_eixo = eixoId;
    this.metaService.editar(event.newData.id, event.newData).subscribe(
      data => {
        // event.newData.id = 2;
        // const msg: string = "Cadastro realizado com sucesso.";
      },
      err => {
        let msg: string = "Tente novamente em instantes.";
        msg.concat(err.error.message);
      }
    );
    
    event.confirm.resolve();
  }
  removerMeta(event){
    console.log(event);
    if (window.confirm('Tem certeza que deseja remover a Meta?')) {
      this.metaService.remover(event.data.id).subscribe();
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  createComponent(Id: number) {
    //this.entry.clear();
    if (Id == 1) {
      const factory = this.resolver.resolveComponentFactory(CmpTextoComponent);
      this.componentRef = this.entry.createComponent(factory);
    } else if (Id == 2) {
      const factory = this.resolver.resolveComponentFactory(CmpTextareaComponent);
      this.componentRef = this.entry.createComponent(factory);
    } 
    this.componentRef.instance.message = "Called by appComponent";
  }

}
