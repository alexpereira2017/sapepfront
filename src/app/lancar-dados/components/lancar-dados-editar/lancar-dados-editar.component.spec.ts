import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LancarDadosEditarComponent } from './lancar-dados-editar.component';

describe('LancarDadosEditarComponent', () => {
  let component: LancarDadosEditarComponent;
  let fixture: ComponentFixture<LancarDadosEditarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LancarDadosEditarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LancarDadosEditarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
