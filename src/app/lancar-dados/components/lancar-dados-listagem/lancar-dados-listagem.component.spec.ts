import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LancarDadosListagemComponent } from './lancar-dados-listagem.component';

describe('LancarDadosListagemComponent', () => {
  let component: LancarDadosListagemComponent;
  let fixture: ComponentFixture<LancarDadosListagemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LancarDadosListagemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LancarDadosListagemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
