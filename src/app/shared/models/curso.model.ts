import { Conceito } from './conceito.model';
import { Autoavaliacao } from './autoavaliacao.model';
import { Responsavel } from './responsavel.model';

export class Curso {
    constructor(
        public nome: string,
        public status: string,
        public criacao: string,
        public conceitos: Conceito[],
        public autoavaliacoes: Autoavaliacao[],
        public responsaveis: Responsavel[],
        public id?: string
    ) {}
}