import { Eixo } from './eixo.model';

export class Meta {

    constructor(
        public meta: string,
        public pontoFraco: string,
        public pontoForte: string,
        public ameacaInterna: string,
        public ameacaExterna: string,
        public viabilidade: string,
        public prioridade: string,
        public alcancado: number,
        public cod_eixo?: number,
        public eixo?: Eixo,
        public id?:string
        ){}
}