import { Autoavaliacao } from './autoavaliacao.model';
import { Indicador } from './indicador.model';
import { Meta } from './meta.model';

export class Eixo {
    constructor()
    constructor(
        public nome?: string,
        public descricao?: string,
        public ordem?: string,
        public criacao?: string,
        // public autoavaliacao?: Autoavaliacao,
        // public indicadores?: Indicador[],
        public indicadoresAgrupados?: Indicador[][],
        public metas?: Meta[],
        public id?: string
    ){}
}