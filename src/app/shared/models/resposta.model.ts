import { Indicador } from './indicador.model'

export class Resposta {
    constructor(
        public ano: string,
        public descritiva: string,
        public valor: string,
        public criacao: string,
        // public indicador?: string,
        public cod_indicador?: string,
        public id?: string
    ){}
}