import { Curso } from './curso.model';

export class Programa {
    constructor()
    constructor(
        public nome?: string,
        public objetivo?: string,
        public status?: string,
        public inicio?: string,
        public termino?: string,
        public criacao?: string,
        public cod_curso?: String,
        public curso?: Curso,
        public id?: string
    ) {}
}