import { Curso } from './curso.model';

export class Conceito {
    constructor(
        public ano: string,
        public nota: string,
        public criacao: string,
        public curso: Curso,
        public id?: string
    ) {}
}