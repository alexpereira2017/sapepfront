import {Usuario} from './usuario.model'
import {Curso} from './curso.model'

export class Responsavel {
    constructor(
        public criacao: string,
        public curso?: Curso,
        public usuario?: Usuario,
        public id?: string
    ){}
}