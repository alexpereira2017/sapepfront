export class Usuario{
    constructor(
        public nome: string,
        public email: string,
        public telefone: string,
        public senha: string,
        public status: string,
        public criacao: string,
        public id?: string
    ) {}
}