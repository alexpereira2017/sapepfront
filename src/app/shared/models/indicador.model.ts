import { Eixo } from './eixo.model';
import { Resposta } from './resposta.model';

export class Indicador {

    constructor(
        public titulo?: string,
        public detalhe?: string,
        public agrupamento?: string,
        public ordem?: string,
        public temporalidade?: string,
        public tipo?: string,
        public criacao?: string,
        public eixo?: Eixo,
        public respostas?: Resposta[],
        public id?:string
        ){}
}