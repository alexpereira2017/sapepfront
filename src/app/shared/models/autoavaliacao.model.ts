import { Eixo } from './eixo.model';

export class Autoavaliacao {
    constructor(
        public nome: string,
        public objetivo: string,
        public status: string,
        public inicio: string,
        public termino: string,
        public Criacao: string,
        public anos: number[],
        public eixos?: Eixo[],
        public id?: string
    ) {}
}