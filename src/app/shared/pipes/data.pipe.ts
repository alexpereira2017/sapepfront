import { Pipe, PipeTransform } from '@angular/core';

import * as moment from 'moment';

@Pipe({
  name: 'data'
})
export class DataPipe implements PipeTransform {

  transform(data: any, args?: any): string {
    let formato = 'DD/MM/YYYY';
    if (args != undefined) {
      formato = args;
    }
    return moment(data).format(formato);
  }

}
