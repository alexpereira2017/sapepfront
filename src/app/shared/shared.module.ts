import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VMessageComponent } from './components/vmessage/vmessage.component';
import { DataPipe } from './';
import { CmpTextoComponent } from './components/cmp-indicadores/cmp-texto/cmp-texto.component';
import { CmpTextareaComponent } from './components/cmp-indicadores/cmp-textarea/cmp-textarea.component';
import { CmpNumeroComponent } from './components/cmp-indicadores/cmp-numero/cmp-numero.component';

@NgModule({
  

  imports: [
    CommonModule
  ],
  declarations: [
    DataPipe,
    CmpTextoComponent,
    CmpTextareaComponent,
    CmpNumeroComponent
  ],
  exports: [
    DataPipe
  ]

 
})
export class SharedModule { }
