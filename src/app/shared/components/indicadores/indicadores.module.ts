import { NgModule } from '@angular/core';
import { GridComponent } from './grid.component';
import { GridItemComponent } from './grid-item.component';
import { CardTemplateBaseComponent, CardStyle1Component, CardStyle2Component, CardStyle3Component, CardStyle4Component, CardStyle5Component, CardStyle6Component } from './card-templates';
import { CommonModule } from '@angular/common';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    declarations: [ 
        GridComponent,
        GridItemComponent,
        CardTemplateBaseComponent,
        CardStyle1Component,
        CardStyle2Component,
        CardStyle3Component,
        CardStyle4Component,
        CardStyle5Component,
        CardStyle6Component

     ],
    exports: [ 
        GridComponent,
        GridItemComponent,
        CardTemplateBaseComponent,
        CardStyle1Component,
        CardStyle2Component,
        CardStyle3Component,
        CardStyle4Component,
        CardStyle5Component,
        CardStyle6Component
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule
    ],
    entryComponents: [
        CardStyle1Component, 
        CardStyle2Component, 
        CardStyle3Component, 
        CardStyle4Component, 
        CardStyle5Component, 
        CardStyle6Component
      ],


})
export class IndicadoresModule { }