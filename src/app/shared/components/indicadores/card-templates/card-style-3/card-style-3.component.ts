import {Component} from '@angular/core';
import {CardTemplateBaseComponent} from '../card-template-base';

@Component({
  selector: 'app-card-style-3',
  templateUrl: './card-style-3.component.html'
})
export class CardStyle3Component extends CardTemplateBaseComponent {}
