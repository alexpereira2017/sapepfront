import {Component} from '@angular/core';
import {CardTemplateBaseComponent} from '../card-template-base';

@Component({
  selector: 'app-card-style-1',
  templateUrl: './card-style-1.component.html'
})
export class CardStyle1Component extends CardTemplateBaseComponent {}
