import {Component} from '@angular/core';
import {CardTemplateBaseComponent} from '../card-template-base';

@Component({
  selector: 'app-card-style-4',
  templateUrl: './card-style-4.component.html'
})
export class CardStyle4Component extends CardTemplateBaseComponent {}
