import {Component} from '@angular/core';
import {CardTemplateBaseComponent} from '../card-template-base';

@Component({
  selector: 'app-card-style-6',
  templateUrl: './card-style-6.component.html'
})
export class CardStyle6Component extends CardTemplateBaseComponent {}
