import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html'
})
export class GridComponent {

  @Input() form: any;
  @Input() items: any;
  @Input() anos: any;
  objetoItems: any;

  constructor() {}
  ngOnInit() {
    this.objetoItems = Object.values(this.items);
  }
}
