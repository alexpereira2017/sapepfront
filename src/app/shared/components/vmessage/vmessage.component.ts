import { Component, Input } from '@angular/core';

@Component({
    selector: 'sap-vmessage',
    templateUrl: './vmessage.component.html'
})
export class VMessageComponent {

    @Input() mensagem = '';
 }