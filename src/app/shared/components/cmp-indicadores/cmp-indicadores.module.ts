import { NgModule } from '@angular/core';

import { CmpNumeroComponent } from './cmp-numero';
import { CmpTextareaComponent } from './cmp-textarea';
import { CmpTextoComponent } from './cmp-texto';
import { CmpIndicadoresComponent } from './cmp-indicadores.component';


@NgModule({
    declarations: [ 
        CmpNumeroComponent, 
        CmpTextareaComponent, 
        CmpTextoComponent,
        CmpIndicadoresComponent
    ],
    exports: [ 
        CmpNumeroComponent, 
        CmpTextareaComponent, 
        CmpTextoComponent,
        CmpIndicadoresComponent
    ]
})
export class CmpModule { }