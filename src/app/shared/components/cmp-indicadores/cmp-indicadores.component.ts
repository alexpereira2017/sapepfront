import {Component, Input} from '@angular/core';

@Component({
  selector: 'indicadores-grid',
  templateUrl: './cmp-indicadores.component.html'
})
export class CmpIndicadoresComponent {

  @Input() items: any;

  constructor() {}
}
