import { Injectable } from '@angular/core';
import { HttpUtilService } from './http-util.service';
import { Eixo } from '../models';
import { environment as env } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EixoService {

  private readonly PATH: string = 'eixo';

  constructor(
  	private http: HttpClient,
    private httpUtil: HttpUtilService) { }

    cadastrar(eixo: Eixo, id?: string) {
      if (id != undefined) {
        return this.editar(id, eixo);
      }      
      return this.inserir(eixo);
    }

    inserir(eixo: Eixo) {
      return this.http.post(
        env.baseApiUrl + this.PATH, 
        eixo, 
        this.httpUtil.headers());
    }

    listarTodos(): Observable<any> {
      return this.http.get(
  	  	env.baseApiUrl + this.PATH,
  	  	this.httpUtil.headers()
  	  );
    }

    listar(id): Observable<any> {
      return this.http.get(
  	  	env.baseApiUrl + this.PATH +'/'+id,
  	  	this.httpUtil.headers()
  	  );
    }

    listarDetalhado(id) {
      return this.http.get(
  	  	env.baseApiUrl + this.PATH +'/detalhado/'+id,
  	  	this.httpUtil.headers()
  	  );
    }

    editar(id, eixo: Eixo) {
      return this.http.put(
        env.baseApiUrl + this.PATH + '/'+id, 
        eixo, 
        this.httpUtil.headers());
    }

    remover(id) {
      return this.http.delete(
        env.baseApiUrl + this.PATH + '/'+id, this.httpUtil.headers());
    }
}
