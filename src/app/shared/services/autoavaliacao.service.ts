import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpUtilService } from './http-util.service';
import { environment as env } from '../../../environments/environment';
import { Programa } from '../models/programa.model';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AutoavaliacaoService {

  private readonly PATH: string = 'autoavaliacao';

  constructor(
  	private http: HttpClient,
    private httpUtil: HttpUtilService) { }

    cadastrar(programa: Programa, id?: string) {
      if (id != undefined) {
        return this.editar(id, programa);
      }      
      return this.inserir(programa);
    }

    inserir(programa: Programa) {
      return this.http.post(
        env.baseApiUrl + this.PATH, 
        programa, 
        this.httpUtil.headers());
    }

    listarTodos(): Observable<any> {
      return this.http.get(
  	  	env.baseApiUrl + this.PATH,
  	  	this.httpUtil.headers()
  	  );
    }

    listar(id): Observable<any> {
      return this.http.get(
  	  	env.baseApiUrl + this.PATH +'/'+id,
  	  	this.httpUtil.headers()
  	  );
    }

    listarDetalhado(id) {
      return this.http.get(
  	  	env.baseApiUrl + this.PATH +'/detalhado/'+id,
  	  	this.httpUtil.headers()
  	  );
    }

    editar(id, programa: Programa) {
      return this.http.put(
        env.baseApiUrl + this.PATH + '/'+id, 
        programa, 
        this.httpUtil.headers());
    }

    remover(id) {
      return this.http.delete(
        env.baseApiUrl + this.PATH + '/'+id, this.httpUtil.headers());
    }
}
