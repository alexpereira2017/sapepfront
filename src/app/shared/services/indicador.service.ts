import { Injectable } from '@angular/core';
import { HttpUtilService } from './http-util.service';
import { Indicador } from '../models';
import { environment as env } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class IndicadorService {

  private readonly PATH: string = 'indicador';

  constructor(
  	private http: HttpClient,
    private httpUtil: HttpUtilService) { }

    cadastrar(indicador: Indicador, id?: string) {
      if (id != undefined) {
        return this.editar(id, indicador);
      }      
      return this.inserir(indicador);
    }

    inserir(indicador: Indicador) {
      return this.http.post(
        env.baseApiUrl + this.PATH, 
        indicador, 
        this.httpUtil.headers());
    }

    listarTodos(): Observable<any> {
      return this.http.get(
  	  	env.baseApiUrl + this.PATH,
  	  	this.httpUtil.headers()
  	  );
    }

    listar(id): Observable<any> {
      return this.http.get(
  	  	env.baseApiUrl + this.PATH +'/'+id,
  	  	this.httpUtil.headers()
  	  );
    }

    listarDetalhado(id) {
      return this.http.get(
  	  	env.baseApiUrl + this.PATH +'/detalhado/'+id,
  	  	this.httpUtil.headers()
  	  );
    }

    editar(id, indicador: Indicador) {
      return this.http.put(
        env.baseApiUrl + this.PATH + '/'+id, 
        indicador, 
        this.httpUtil.headers());
    }

    remover(id) {
      return this.http.delete(
        env.baseApiUrl + this.PATH + '/'+id, this.httpUtil.headers());
    }
}