import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpUtilService } from './http-util.service';
import { Resposta } from '../models';
import { environment as env } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RespostaService {

  private readonly PATH: string = 'resposta';

  constructor(
  	private http: HttpClient,
    private httpUtil: HttpUtilService) { }

    salvar(resposta: Resposta[], id?: string) {
      return this.http.put(
        env.baseApiUrl + this.PATH, 
        resposta, 
        this.httpUtil.headers());
    }
}
