import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { environment as env } from '../../../environments/environment';

import { Curso } from '../models';
import { HttpUtilService } from './http-util.service';

@Injectable({
  providedIn: 'root'
})
export class CursoService {

  private readonly PATH: string = 'curso';

  constructor(
  	private http: HttpClient,
    private httpUtil: HttpUtilService) { }
    
    cadastrarCurso(curso: Curso) {
      return this.inserir(curso);
    }

    inserir(curso: Curso) {
      return this.http.post(env.baseApiUrl + this.PATH, curso, this.httpUtil.headers());
    }

    listarTodosCursos(): Observable<any> {
      return this.http.get(
  	  	env.baseApiUrl + this.PATH,
  	  	this.httpUtil.headers()
  	  );
    }

    listarCurso(id) {
      return this.http.get(
  	  	env.baseApiUrl + this.PATH +'/'+id,
  	  	this.httpUtil.headers()
  	  );
    }

    editarCurso(id, curso: Curso) {
      return this.http.put(env.baseApiUrl + this.PATH + '/'+id, curso, this.httpUtil.headers());
    }
    
    removerCurso(id) {
      return this.http.delete(env.baseApiUrl + this.PATH + '/'+id, this.httpUtil.headers());
    }
}
