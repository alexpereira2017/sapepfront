import { Injectable } from '@angular/core';
import { HttpUtilService } from './http-util.service';
import { Meta } from '../models';
import { environment as env } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class MetaService {

  private readonly PATH: string = 'meta';

  constructor(
  	private http: HttpClient,
    private httpUtil: HttpUtilService) { }

    cadastrar(meta: Meta, id?: string) {
      if (id != undefined) {
        return this.editar(id, meta);
      }      
      return this.inserir(meta);
    }

    inserir(meta: Meta) {
      return this.http.post(
        env.baseApiUrl + this.PATH, 
        meta, 
        this.httpUtil.headers());
    }

    listarTodos(): Observable<any> {
      return this.http.get(
  	  	env.baseApiUrl + this.PATH,
  	  	this.httpUtil.headers()
  	  );
    }

    listar(id): Observable<any> {
      return this.http.get(
  	  	env.baseApiUrl + this.PATH +'/'+id,
  	  	this.httpUtil.headers()
  	  );
    }

    listarPorEixo(id) {
      return this.http.get(
  	  	env.baseApiUrl + this.PATH +'/por-eixo/'+id,
  	  	this.httpUtil.headers()
  	  );
    }

    editar(id, meta: Meta) {
      return this.http.put(
        env.baseApiUrl + this.PATH + '/'+id, 
        meta, 
        this.httpUtil.headers());
    }

    remover(id) {
      console.log(env.baseApiUrl + this.PATH + '/'+id);
      return this.http.delete(
        env.baseApiUrl + this.PATH + '/'+id, this.httpUtil.headers());
    }
}
