import { Injectable } from '@angular/core';
import { HttpUtilService } from './http-util.service';
import { Login } from '../models';
import { environment as env } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private readonly PATH: string = 'login';

  constructor(
  	private http: HttpClient,
    private httpUtil: HttpUtilService) { }

    logar(login: Login): Observable<any> {
      return this.http.post(
        env.baseApiUrl + this.PATH, 
        login,
        {observe: 'response' as 'body'}
      );
    }
}
