import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {
    AcoesAvaliarComponent,
    AcoesListagemComponent,
    AcoesComponent
} from './components';

export const AcoesRoutes: Routes = [
    {
      path: 'acoes',
      component: AcoesComponent,
      children: [
        {
          path: 'avaliar', 
          component: AcoesAvaliarComponent 
        },
        {
          path: 'listagem', 
          component: AcoesListagemComponent
        }
      ]
    }
];

@NgModule({
    imports: [
      RouterModule.forChild(AcoesRoutes)
    ],
    exports: [
      RouterModule
    ]
  })
  export class AcoesRoutingModule {
  }
  