import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AcoesAvaliarComponent } from './components/acoes-avaliar/acoes-avaliar.component';
import { AcoesListagemComponent } from './components/acoes-listagem/acoes-listagem.component';
import { AcoesComponent } from './components';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [AcoesAvaliarComponent, AcoesListagemComponent, AcoesComponent],
  imports: [
    CommonModule,
    RouterModule
  ]
})
export class AcoesModule { }
