import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcoesAvaliarComponent } from './acoes-avaliar.component';

describe('AcoesAvaliarComponent', () => {
  let component: AcoesAvaliarComponent;
  let fixture: ComponentFixture<AcoesAvaliarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcoesAvaliarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcoesAvaliarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
