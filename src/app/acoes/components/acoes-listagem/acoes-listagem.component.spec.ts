import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcoesListagemComponent } from './acoes-listagem.component';

describe('AcoesListagemComponent', () => {
  let component: AcoesListagemComponent;
  let fixture: ComponentFixture<AcoesListagemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcoesListagemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcoesListagemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
