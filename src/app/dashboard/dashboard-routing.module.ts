import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {
    DashboardGraficoComponent,
    DashboardComponent
} from './components';

export const DashboardRoutes: Routes = [
    {
      path: 'dashboard',
      component: DashboardComponent,
      children: [
        {
          path: '', 
          component: DashboardGraficoComponent 
        }
      ]
    }
];

@NgModule({
    imports: [
      RouterModule.forChild(DashboardRoutes)
    ],
    exports: [
      RouterModule
    ]
  })
  export class DashboardRoutingModule {
  }
  