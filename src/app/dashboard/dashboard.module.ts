import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { DashboardGraficoComponent } from './components/dashboard-grafico/dashboard-grafico.component';
import { DashboardComponent } from './components';


// import { 
//   HttpUtilService, 
//   LancamentoService
// } from '../shared';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ], 
  declarations: [
    DashboardComponent,
    DashboardGraficoComponent
  ]
})
export class DashboardModule { }
