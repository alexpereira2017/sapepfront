import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EixoCadastroComponent } from './eixo-cadastro.component';

describe('EixoCadastroComponent', () => {
  let component: EixoCadastroComponent;
  let fixture: ComponentFixture<EixoCadastroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EixoCadastroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EixoCadastroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
