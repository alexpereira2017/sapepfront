import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EixoService } from 'src/app/shared/services/eixo.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { of } from 'rxjs';
import { AutoavaliacaoService, DataPipe } from 'src/app/shared';
import { Programa, Eixo } from 'src/app/shared/models';

@Component({
  selector: 'app-eixo-cadastro',
  templateUrl: './eixo-cadastro.component.html',
  styleUrls: ['./eixo-cadastro.component.css']
})
export class EixoCadastroComponent implements OnInit {

  FRMcadastro: FormGroup;
  eixo: Eixo;
  programas: Programa[];
  id?: string;

  constructor( 
    private router: Router,
    private route: ActivatedRoute,
    private eixoService: EixoService,
    private programaService: AutoavaliacaoService,
    private formBuilder: FormBuilder
    ) { }

    ngOnInit() {

      this.id = this.route.snapshot.params['id'];
      this.inicializar(this.id);

      this.FRMcadastro = this.formBuilder.group({
        nome: ['', [Validators.required, Validators.minLength(5)]],
        descricao: [''],
        ordem: [''],
        cod_autoavaliacao: ['', Validators.required]
      });

      of(this.buscarTodosProgramas()).subscribe(dados => {
        this.programas = dados;
      });
  }

  buscarTodosProgramas() {
    this.programaService.listarTodos()
      .subscribe((data: Programa[]) => {
        this.programas = data;
      });
      return this.programas;
  }

  inicializar(id?) {
    this.eixo = new Eixo();
    if(id) {
      this.eixoService.listar(id)
      .subscribe(
          dados => {
            this.FRMcadastro.get('nome').setValue(dados.nome);
            this.FRMcadastro.get('descricao').setValue(dados.descricao);
            this.FRMcadastro.get('ordem').setValue(dados.ordem);
            this.FRMcadastro.get('cod_autoavaliacao').setValue(dados.autoavaliacao.id);
          },
          err => {
            let msg: string = "Erro ao obter dados do Eixo";
            this.voltar();
          }
        );
      }
  }

  cadastrar() {
    const eixo: Eixo = this.FRMcadastro.value;

    if (this.FRMcadastro.invalid) {
      return false;
    }
    this.eixoService.cadastrar(eixo, this.route.snapshot.params['id'])
      .subscribe(
        data => {
          const msg: string = "Cadastro realizado com sucesso.";
          if (!this.id) {
            this.router.navigate(['/eixo/listagem']);
          }
        },
        err => {
          let msg: string = "Tente novamente em instantes.";
          if (err.status == 400) {
            msg = err.error.errors.join(' ');
          }
        }
      );
  	return false;
  }

  voltar() {
    // this.router.navigate(['/eixo/listagem']);
    this.router.navigateByUrl('/eixo/listagem');
  }

}
