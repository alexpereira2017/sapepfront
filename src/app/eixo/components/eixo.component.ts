import { Component } from '@angular/core';

@Component({
  template: `
  <div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Eixos</h1>
        </div>
        <!-- /.col-lg-12 -->
        <router-outlet></router-outlet>
    </div>
  </div>`
})
export class EixoComponent {
}
