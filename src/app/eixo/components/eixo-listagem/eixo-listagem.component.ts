import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { Eixo } from 'src/app/shared/models';
import { EixoService } from 'src/app/shared';

@Component({
  selector: 'app-eixo-listagem',
  templateUrl: './eixo-listagem.component.html',
  styleUrls: ['./eixo-listagem.component.css']
})
export class EixoListagemComponent implements OnInit {

  dataSourse: MatTableDataSource<Eixo>
  eixos: Eixo[];

  constructor(
    private router: Router,
    private eixoService: EixoService

  ) { }

  ngOnInit() {
    this.buscarTodos();
  }

  direcionarParaCadastro() {
    this.router.navigate(['/eixo/cadastro']);
  }

  buscarTodos() {
    this.eixoService.listarTodos()
      .subscribe((data: Eixo[]) => {
        this.eixos = data;
      });
  }

  remover(eixo: Eixo) {
    this.eixoService.remover(eixo.id)
    .subscribe( data => {
      this.eixos = this.eixos.filter( u => u !== eixo);
    });
  }

}
