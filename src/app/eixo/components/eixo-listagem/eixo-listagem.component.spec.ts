import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EixoListagemComponent } from './eixo-listagem.component';

describe('EixoListagemComponent', () => {
  let component: EixoListagemComponent;
  let fixture: ComponentFixture<EixoListagemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EixoListagemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EixoListagemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
