import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EixoCadastroComponent } from './components/eixo-cadastro/eixo-cadastro.component';
import { EixoListagemComponent } from './components/eixo-listagem/eixo-listagem.component';
import { RouterModule } from '@angular/router';
import { EixoComponent } from './components';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { VMessageModule } from '../shared/components/vmessage/vmessage.module';
import { SharedModule } from '../shared/shared.module';



@NgModule({
  declarations: [
    EixoCadastroComponent, 
    EixoListagemComponent,
    EixoComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    VMessageModule,
    SharedModule,
    ReactiveFormsModule
  ]
})
export class EixoModule { }
