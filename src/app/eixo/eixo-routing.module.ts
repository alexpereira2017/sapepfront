import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {
    EixoCadastroComponent,
    EixoListagemComponent,
    EixoComponent
} from './components';

export const EixoRoutes: Routes = [
    {
      path: 'eixo',
      component: EixoComponent,
      children: [
        {
          path: 'listagem', 
          component: EixoListagemComponent
        },
        {
          path: 'cadastro', 
          component: EixoCadastroComponent 
        },
        {
          path: 'editar/:id',
          component: EixoCadastroComponent
        },
      ]
    }
];

@NgModule({
    imports: [
      RouterModule.forChild(EixoRoutes)
    ],
    exports: [
      RouterModule
    ]
  })
  export class EixoRoutingModule {
  }
  