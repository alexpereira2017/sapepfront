import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {
    LoginComponent,
    LogoutComponent,
    AutenticacaoComponent
} from './components';

export const AutenticacaoRoutes: Routes = [
    {
      path: 'autenticacao',
      component: AutenticacaoComponent,
      children: [
        {
          path: 'login', 
          component: LoginComponent
        },
        {
          path: 'logout', 
          component: LogoutComponent 
        },
      ]
    }
];

@NgModule({
    imports: [
      RouterModule.forChild(AutenticacaoRoutes)
    ],
    exports: [
      RouterModule
    ]
  })
  export class AutenticacaoRoutingModule {
  }
  