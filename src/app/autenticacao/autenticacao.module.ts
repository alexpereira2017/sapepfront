import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './components/login/login.component';
import { AutenticacaoComponent } from './components';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VMessageModule } from '../shared/components/vmessage/vmessage.module';
import { SharedModule } from '../shared/shared.module';
import { LogoutComponent } from './components/logout/logout.component';

@NgModule({
  declarations: [
    LoginComponent,
    LogoutComponent,
    AutenticacaoComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    VMessageModule,
    SharedModule,
    ReactiveFormsModule
  ]
})
export class AutenticacaoModule { }
