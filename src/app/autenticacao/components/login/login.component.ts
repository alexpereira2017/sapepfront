import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/shared/services/login.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Login } from 'src/app/shared/models';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  FRMcadastro: FormGroup;
  msg: String;

  constructor( 
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private service: LoginService
    ) { }
  

  ngOnInit() {
    this.direcionarCasoEstejaLogado();
    this.FRMcadastro = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      senha: ['', [Validators.required, Validators.minLength(5)]]
    });
  }

  direcionarCasoEstejaLogado() {
    if(localStorage['token']) {
      this.router.navigate(['/dashboard']);
    }
  }

  logar() {
    if (this.FRMcadastro.invalid) {
      return;
    }
    const login: Login = this.FRMcadastro.value;
    this.service.logar(login)
      .subscribe(
        data => {
          localStorage['token'] = data.headers.get('authorization');
          // const usuarioData = JSON.parse(atob(data['data']['token'].split('.')[1]));
            this.router.navigate(['/dashboard']);
          },
          err => {
            this.msg = "Tente novamente em instantes.";
            if (err['status'] == 401) {
              this.msg = "Email/senha inválido(s)."
            }
            // this.snackBar.open(msg, "Erro", { duration: 5000 });
          }
          );
        }
}
