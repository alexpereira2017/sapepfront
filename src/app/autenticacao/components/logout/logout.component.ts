import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private cdRef:ChangeDetectorRef
  ) { }


  ngOnInit() {
    this.deslogar();
  }

  deslogar() {
    this.removerToken();
    this.router.navigate(['/autenticacao/login']);
  }

  removerToken() {
    this.cdRef.detectChanges();
    localStorage['token'] = '';
  }

}
