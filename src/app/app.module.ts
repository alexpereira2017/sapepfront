import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { ErrorsModule } from './errors/errors.module';
import { DashboardRoutingModule } from './dashboard';
import { CursoRoutingModule, CursoModule } from './curso';
import { AcoesModule, AcoesRoutingModule } from './acoes';
import { ProgramaModule, ProgramaRoutingModule } from './programa';
import { EixoModule, EixoRoutingModule } from './eixo';
import { IndicadorModule, IndicadorRoutes, IndicadorRoutingModule } from './indicador';
import { UsuarioModule, UsuarioRoutingModule } from './usuario';
import { HttpClientModule } from '@angular/common/http'; 
import { OverlayModule } from "@angular/cdk/overlay";
import { MatSnackBarModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LancarDadosModule, LancarDadosRoutingModule } from './lancar-dados';
import { AutenticacaoModule, AutenticacaoRoutingModule } from './autenticacao';




@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    ErrorsModule,
    DashboardModule,
    DashboardRoutingModule,
    AcoesModule,
    AcoesRoutingModule,
    CursoModule,
    CursoRoutingModule,
    ProgramaModule,
    ProgramaRoutingModule,
    EixoModule,
    EixoRoutingModule,
    IndicadorModule,
    IndicadorRoutingModule,
    UsuarioModule,
    UsuarioRoutingModule,
    LancarDadosModule,
    LancarDadosRoutingModule,
    AutenticacaoModule,
    AutenticacaoRoutingModule,
    HttpClientModule,
    OverlayModule,
    MatSnackBarModule,
    BrowserAnimationsModule,

    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
