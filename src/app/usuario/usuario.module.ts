import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsuarioCadastroComponent } from './components/usuario-cadastro/usuario-cadastro.component';
import { UsuarioListagemComponent } from './components/usuario-listagem/usuario-listagem.component';
import { UsuarioComponent } from './components';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    UsuarioCadastroComponent, 
    UsuarioListagemComponent,
    UsuarioComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ]
})
export class UsuarioModule { }
