import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {
    UsuarioCadastroComponent,
    UsuarioListagemComponent,
    UsuarioComponent
} from './components';

export const UsuarioRoutes: Routes = [
    {
      path: 'usuario',
      component: UsuarioComponent,
      children: [
        {
          path: 'listagem', 
          component: UsuarioListagemComponent
        },
        {
          path: 'cadastro', 
          component: UsuarioCadastroComponent 
        }
      ]
    }
];

@NgModule({
    imports: [
      RouterModule.forChild(UsuarioRoutes)
    ],
    exports: [
      RouterModule
    ]
  })
  export class UsuarioRoutingModule {
  }
  