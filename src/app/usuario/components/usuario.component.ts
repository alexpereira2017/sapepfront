import { Component } from '@angular/core';

@Component({
  template: `
  	<h2 fxLayoutAlign="center">Usuario</h2>
  	<router-outlet></router-outlet>
  `
})
export class UsuarioComponent {
}
