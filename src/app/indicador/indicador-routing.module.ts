import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {
    IndicadorCadastroComponent,
    IndicadorListagemComponent,
    IndicadorComponent
} from './components';

export const IndicadorRoutes: Routes = [
    {
      path: 'indicador',
      component: IndicadorComponent,
      children: [
        {
          path: 'listagem', 
          component: IndicadorListagemComponent
        },
        {
          path: 'cadastro', 
          component: IndicadorCadastroComponent 
        }
      ]
    }
];

@NgModule({
    imports: [
      RouterModule.forChild(IndicadorRoutes)
    ],
    exports: [
      RouterModule
    ]
  })
  export class IndicadorRoutingModule {
  }
  