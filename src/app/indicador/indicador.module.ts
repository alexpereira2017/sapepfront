import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IndicadorListagemComponent } from './components/indicador-listagem/indicador-listagem.component';
import { IndicadorCadastroComponent } from './components/indicador-cadastro/indicador-cadastro.component';
import { IndicadorComponent } from './components';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { VMessageModule } from '../shared/components/vmessage/vmessage.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    IndicadorListagemComponent, 
    IndicadorCadastroComponent,
    IndicadorComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    VMessageModule,
    SharedModule,
    ReactiveFormsModule
  ]
})
export class IndicadorModule { }
