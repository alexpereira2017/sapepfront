import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndicadorCadastroComponent } from './indicador-cadastro.component';

describe('IndicadorCadastroComponent', () => {
  let component: IndicadorCadastroComponent;
  let fixture: ComponentFixture<IndicadorCadastroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndicadorCadastroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndicadorCadastroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
