import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { IndicadorService } from 'src/app/shared/services/indicador.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { of } from 'rxjs';
import { AutoavaliacaoService, DataPipe } from 'src/app/shared';
import { Programa, Indicador } from 'src/app/shared/models';

@Component({
  selector: 'app-indicador-cadastro',
  templateUrl: './indicador-cadastro.component.html',
  styleUrls: ['./indicador-cadastro.component.css']
})
export class IndicadorCadastroComponent implements OnInit {

  FRMcadastro: FormGroup;
  indicador: Indicador;
  programas: Programa[];
  id?: string;

  constructor( 
    private router: Router,
    private route: ActivatedRoute,
    private indicadorService: IndicadorService,
    private programaService: AutoavaliacaoService,
    private formBuilder: FormBuilder
    ) { }

    ngOnInit() {

      this.id = this.route.snapshot.params['id'];
      this.inicializar(this.id);

      this.FRMcadastro = this.formBuilder.group({
        nome: ['', [Validators.required, Validators.minLength(5)]],
        descricao: [''],
        ordem: [''],
        cod_autoavaliacao: ['', Validators.required]
      });

      of(this.buscarTodosProgramas()).subscribe(dados => {
        this.programas = dados;
      });
  }

  buscarTodosProgramas() {
    this.programaService.listarTodos()
      .subscribe((data: Programa[]) => {
        this.programas = data;
      });
      return this.programas;
  }

  inicializar(id?) {
    this.indicador = new Indicador();
    if(id) {
      this.indicadorService.listar(id)
      .subscribe(
          dados => {
            this.FRMcadastro.get('nome').setValue(dados.nome);
            this.FRMcadastro.get('descricao').setValue(dados.descricao);
            this.FRMcadastro.get('ordem').setValue(dados.ordem);
            this.FRMcadastro.get('cod_autoavaliacao').setValue(dados.autoavaliacao.id);
          },
          err => {
            let msg: string = "Erro ao obter dados do Indicador";
            this.voltar();
          }
        );
      }
  }

  cadastrar() {
    const indicador: Indicador = this.FRMcadastro.value;

    if (this.FRMcadastro.invalid) {
      return false;
    }
    this.indicadorService.cadastrar(indicador, this.route.snapshot.params['id'])
      .subscribe(
        data => {
          const msg: string = "Cadastro realizado com sucesso.";
          if (!this.id) {
            this.router.navigate(['/indicador/listagem']);
          }
        },
        err => {
          let msg: string = "Tente novamente em instantes.";
          if (err.status == 400) {
            msg = err.error.errors.join(' ');
          }
        }
      );
  	return false;
  }

  voltar() {
    this.router.navigate(['/indicador/listagem']);
  }

}
