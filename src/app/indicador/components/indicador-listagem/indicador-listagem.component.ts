import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { Indicador } from 'src/app/shared/models';
import { IndicadorService } from 'src/app/shared';

@Component({
  selector: 'app-indicador-listagem',
  templateUrl: './indicador-listagem.component.html',
  styleUrls: ['./indicador-listagem.component.css']
})
export class IndicadorListagemComponent implements OnInit {

  dataSourse: MatTableDataSource<Indicador>
  indicadors: Indicador[];

  constructor(
    private router: Router,
    private indicadorService: IndicadorService

  ) { }

  ngOnInit() {
    this.buscarTodos();
  }

  direcionarParaCadastro() {
    this.router.navigate(['/indicador/cadastro']);
  }

  buscarTodos() {
    this.indicadorService.listarTodos()
      .subscribe((data: Indicador[]) => {
        this.indicadors = data;
      });
  }

  remover(indicador: Indicador) {
    this.indicadorService.remover(indicador.id)
    .subscribe( data => {
      this.indicadors = this.indicadors.filter( u => u !== indicador);
    });
  }
}
