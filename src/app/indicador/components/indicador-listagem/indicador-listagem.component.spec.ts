import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndicadorListagemComponent } from './indicador-listagem.component';

describe('IndicadorListagemComponent', () => {
  let component: IndicadorListagemComponent;
  let fixture: ComponentFixture<IndicadorListagemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndicadorListagemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndicadorListagemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
