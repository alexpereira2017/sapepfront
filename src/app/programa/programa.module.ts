import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProgramaListagemComponent } from './components/programa-listagem/programa-listagem.component';
import { ProgramaCadastroComponent } from './components/programa-cadastro/programa-cadastro.component';
import { RouterModule } from '@angular/router';
import { ProgramaComponent } from './components';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { VMessageModule } from '../shared/components/vmessage/vmessage.module';

import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    ProgramaListagemComponent, 
    ProgramaCadastroComponent,
    ProgramaComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    VMessageModule,
    SharedModule,
    ReactiveFormsModule
  ]
})
export class ProgramaModule { }
