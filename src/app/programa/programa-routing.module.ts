import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {
    ProgramaCadastroComponent,
    ProgramaListagemComponent,
    ProgramaComponent
} from './components';

export const ProgramaRoutes: Routes = [
    {
      path: 'programa',
      component: ProgramaComponent,
      children: [
        {
          path: 'listagem', 
          component: ProgramaListagemComponent
        },
        {
          path: 'cadastro', 
          component: ProgramaCadastroComponent 
        },
        {
          path: 'editar/:id',
          component: ProgramaCadastroComponent
        },
      ]
    }
];

@NgModule({
    imports: [
      RouterModule.forChild(ProgramaRoutes)
    ],
    exports: [
      RouterModule
    ]
  })
  export class ProgramaRoutingModule {
  }
  