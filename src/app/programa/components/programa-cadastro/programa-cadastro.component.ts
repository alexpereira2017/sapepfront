import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AutoavaliacaoService } from 'src/app/shared/services/autoavaliacao.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { of } from 'rxjs';
import { CursoService, DataPipe } from 'src/app/shared';
import { Curso } from 'src/app/shared/models';
import { Programa } from 'src/app/shared/models/programa.model';
import { DatePipe } from '@angular/common';


@Component({
  selector: 'app-programa-cadastro',
  templateUrl: './programa-cadastro.component.html',
  styleUrls: ['./programa-cadastro.component.css']
})
export class ProgramaCadastroComponent implements OnInit {

  FRMcadastro: FormGroup;
  cursos: Curso[];
  programa: Programa;
  id?: string;

  constructor( 
    private router: Router,
    private route: ActivatedRoute,
    private autoavaliacaoService: AutoavaliacaoService,
    private cursoService: CursoService,
    private formBuilder: FormBuilder
    ) { }
    
    ngOnInit() {

      this.id = this.route.snapshot.params['id'];
      this.inicializarPrograma(this.id);

      this.FRMcadastro = this.formBuilder.group({
        nome: ['', [Validators.required, Validators.minLength(5)]],
        objetivo: [''],
        situacao: [''],
        inicio: ['', Validators.required],
        termino: ['', Validators.required],
        cod_curso: ['', Validators.required]
      });

    // Assync
    // https://coryrylan.com/blog/creating-a-dynamic-select-with-angular-forms
      of(this.buscarTodosCursos()).subscribe(dados => {
        this.cursos = dados;
      });

      // Sync
      // this.cursos = this.buscarTodosCursos();
  }

  buscarTodosCursos() {
    this.cursoService.listarTodosCursos()
      .subscribe((data: Curso[]) => {
        this.cursos = data;
      });
      return this.cursos;
  }

  inicializarPrograma(id?) {
    this.programa = new Programa();
    if(id) {
      this.autoavaliacaoService.listar(id)
      .subscribe(
          dados => {
            this.FRMcadastro.get('nome').setValue(dados.nome);
            this.FRMcadastro.get('objetivo').setValue(dados.objetivo);
            this.FRMcadastro.get('cod_curso').setValue(dados.curso.id);
            this.FRMcadastro.get('inicio').setValue(new DataPipe().transform(dados.inicio, 'YYYY-MM-DD'));
            this.FRMcadastro.get('termino').setValue(new DataPipe().transform(dados.termino, 'YYYY-MM-DD'));
          },
          err => {
            let msg: string = "Erro obtendo Autoavaliação";
            this.voltar();
          }
        );
      }
  }

  cadastrar() {
    const programa: Programa = this.FRMcadastro.value;
    if (this.FRMcadastro.invalid) {
      return false;
    }
    this.autoavaliacaoService.cadastrar(programa, this.route.snapshot.params['id'])
      .subscribe(
        data => {
          const msg: string = "Cadastro realizado com sucesso.";
          if (!this.id) {
            this.router.navigate(['/programa/listagem']);
          }
        },
        err => {
          let msg: string = "Tente novamente em instantes.";
          if (err.status == 400) {
            msg = err.error.errors.join(' ');
          }
        }
      );
  	return false;
  }

  voltar() {
    this.router.navigate(['/programa/listagem']);
  }

}
