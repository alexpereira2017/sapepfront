import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgramaCadastroComponent } from './programa-cadastro.component';

describe('ProgramaCadastroComponent', () => {
  let component: ProgramaCadastroComponent;
  let fixture: ComponentFixture<ProgramaCadastroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgramaCadastroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgramaCadastroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
