import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { Programa } from 'src/app/shared/models/programa.model';
import { Router } from '@angular/router';
import { AutoavaliacaoService } from 'src/app/shared/services/autoavaliacao.service';

@Component({
  selector: 'app-programa-listagem',
  templateUrl: './programa-listagem.component.html',
  styleUrls: ['./programa-listagem.component.css']
})
export class ProgramaListagemComponent implements OnInit {

  dataSourse: MatTableDataSource<Programa>
  programas: Programa[];

  constructor(
    private router: Router,
    private autoavaliacaoService: AutoavaliacaoService

  ) { }

  ngOnInit() {
    this.buscarTodos();
  }

  direcionarParaCadastro() {
    this.router.navigate(['/programa/cadastro']);
  }

  buscarTodos() {
    this.autoavaliacaoService.listarTodos()
      .subscribe((data: Programa[]) => {
        this.programas = data;
      });
      //   err => {
      //     const msg: string = "Erro obtendo lançamentos.";
      //     this.snackBar.open(msg, "Erro", { duration: 5000 });
      //   }
      // );
  }

  remover(programa: Programa) {
    this.autoavaliacaoService.remover(programa.id)
    .subscribe( data => {
      this.programas = this.programas.filter( u => u !== programa);
    });
  }

}
