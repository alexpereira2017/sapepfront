import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgramaListagemComponent } from './programa-listagem.component';

describe('ProgramaListagemComponent', () => {
  let component: ProgramaListagemComponent;
  let fixture: ComponentFixture<ProgramaListagemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgramaListagemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgramaListagemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
