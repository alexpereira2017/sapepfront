import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

declare var $:any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Sapep';

  constructor(private router: Router) { }

  ngOnInit() {}

  ngAfterViewInit() {
    $(document).ready(function(){
      $('#dataTables').DataTable({
        responsive: true
      });
    });
  }

  sair() {
  	delete localStorage['token'];
  	this.router.navigate(['/']);
  }

  autenticado(): boolean {
    return localStorage['token'];
  }

}
